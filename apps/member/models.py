# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


# Create your models here.
from django.utils.safestring import mark_safe


class Category(models.Model):
    category = models.CharField(max_length=80)

    class Meta:
        pass

    def __str__(self):
        return '{}'.format(self.category)


class CivilStatus(models.Model):
    civil_status = models.CharField(max_length=80)

    class Meta:
        pass

    def __str__(self):
        return '{}'.format(self.civil_status)


class Church(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=100, blank=True)
    actives = models.IntegerField(blank=True)
    retired = models.IntegerField(blank=True)
    temporary = models.IntegerField(blank=True)

    class Meta:
        pass

    def __str__(self):
        return '{}'.format(self.name)


class StatusLegal(models.Model):
    status = models.CharField(max_length=80)

    class Meta:
        pass

    def __str__(self):
        return '{}'.format(self.status)


class Member(models.Model):
    genders = (('Hombre', 'Hombre'), ('Mujer', 'Mujer'))

    name = models.CharField(max_length=100)
    father_last = models.CharField(max_length=100)
    mother_last = models.CharField(max_length=100, blank=True)
    birth_date = models.DateField()
    gender = models.CharField(max_length=80, choices=genders)
    address = models.CharField(max_length=200)
    email = models.EmailField(blank=True)
    entered_church = models.DateField()
    baptism = models.DateField()
    # AGREGAR PAIS DE NACIMIENTO

    # Relationships
    church = models.ForeignKey(Church)
    civil_status = models.ForeignKey(CivilStatus)
    category = models.ForeignKey(Category)
    status_legal = models.ForeignKey(StatusLegal)

    class Meta:
        pass

    def __str__(self):
        return '{} {} {}'.format(self.name, self.father_last, self.mother_last)

    def image_tag(self):
        return mark_safe('<img src="%s" width="150" height="150" />' % self.photo.url)

    image_tag.short_description = 'Image'


