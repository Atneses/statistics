# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from models import Member, Category, Church, CivilStatus, StatusLegal


class MemberAdmin(admin.ModelAdmin):
    readonly_fields = ('image_tag', )

admin.site.register(Category)
admin.site.register(Member, MemberAdmin)
admin.site.register(CivilStatus)
admin.site.register(Church)
admin.site.register(StatusLegal)
