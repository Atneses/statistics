# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from models import PhoneType, Phone

admin.site.register(Phone)
admin.site.register(PhoneType)
