# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


# Create your models here.
class PhoneType(models.Model):
    phone_type = models.CharField(max_length=100)

    class Meta:
        pass

    def __str__(self):
        pass


class Phone(models.Model):
    phone = models.CharField(max_length=100)

    class Meta:
        pass

    def __str__(self):
        pass

    # Relationships
    phone_type = models.ForeignKey(PhoneType)
    member = models.ForeignKey('member.Member')
